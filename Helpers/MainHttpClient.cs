﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Drugs.Helpers
{
    public static class MainHttpClient
    {
        private static string _baseAddress;
        public static HttpClient GetClient()
        {
            HttpClient client = new HttpClient();
            client = new HttpClient { BaseAddress = new Uri(_baseAddress) };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }

        public static string SetBaseAddress(string baseAddress)
        {
            return _baseAddress = baseAddress;
        }
    }
}

﻿namespace Drugs
{
    public class OpenFDAUrl
    {
        public string MainApi { get; set; }
        public DrugEndpoint Endpoint { get; set; }
        public string ApiKey { get;  set; }
    }

    public class DrugEndpoint
    {
        public string Event { get; set; }
        public string Label { get; set; }
        public string Enforcement { get; set; }

    }
}

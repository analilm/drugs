﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Drugs.Entities;
using Drugs.Helpers;
using Drugs.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Drugs.Controllers
{
    public class HomeController : Controller
    {
        private static OpenFDAUrl _openFDA;
        private List<string> _medicinalProducts;
        private List<string> _reactionMedDra;
        private List<string> _pharmEpc;

        public HomeController(IOptions<OpenFDAUrl> openFDA)
        {
            _openFDA = openFDA.Value;
            MainHttpClient.SetBaseAddress(_openFDA.MainApi);
        }


        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> Search(KeywordModel search)
        {
            try
            {
                var client = MainHttpClient.GetClient();
                var response = await client.GetAsync($"{_openFDA.Endpoint.Event}?api_key={_openFDA.ApiKey}&search={search.Keyword}");
                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsStringAsync();
                var rawDrug = Newtonsoft.Json.JsonConvert.DeserializeObject<RawDrug>(result);
                ViewData["Keyword"] = search.Keyword;
                ViewData["RawDrug"] = rawDrug;

                AddMedicinalProductToList(rawDrug);
                AddReactionsToList(rawDrug);
                AddPharmEpcToList(rawDrug);

                ViewData["Disclaimer"] = rawDrug.Meta.Disclaimer;
                ViewData["LastUpdated"] = rawDrug.Meta.LastUpdated;
                ViewData["MedicinalProduct"] = _medicinalProducts;
                ViewData["ReactionMedDra"] = _reactionMedDra;
                ViewData["PharmEpc"] = _pharmEpc;

                return View();
            }
            catch (HttpRequestException ex)
            {
                return BadRequest($"{ex.Message} ==> {search.Keyword}");
            }
        }

        private void AddPharmEpcToList(RawDrug rawDrug)
        {
            var pharmEpc = new List<string>();
            foreach (var r in rawDrug.Results)
            {
                foreach (var d in r.Patient.Drug)
                {
                    if (d.OpenFda != null && d.OpenFda.PharmClassEpc != null)
                    {
                        foreach (var name in d.OpenFda.PharmClassEpc)
                        {
                            pharmEpc.Add(name);
                        }
                    }
                }
            }
            _pharmEpc = pharmEpc.Distinct().ToList();

        }

        private void AddReactionsToList(RawDrug rawDrug)
        {
            var reactionMedDra = new List<string>();
            foreach (var r in rawDrug.Results)
            {
                foreach (var name in r.Patient.Reactions)
                {
                    reactionMedDra.Add(name.ReactionMedDraPt);
                }
            }
            _reactionMedDra = reactionMedDra.Distinct().ToList();
        }

        private void AddMedicinalProductToList(RawDrug rawDrug)
        {
            var medicinalProducts = new List<string>();
            foreach (var r in rawDrug.Results)
            {
                foreach (var name in r.Patient.Drug)
                {
                    medicinalProducts.Add(name.MedicinalProduct);
                }
            }
            _medicinalProducts = medicinalProducts.Distinct().ToList();
        }
    }
}

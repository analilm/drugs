﻿using System.ComponentModel;

namespace Drugs.Models
{
    public class KeywordModel
    {
        [DisplayName("Keyword")]
        public string Keyword { get; set; }
    }
}
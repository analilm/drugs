﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Drugs.Models
{
    public class DrugDto
    {
        public string[] MedicinalProduct { get; set; }
        public string[] ReactionMedDraPt { get; set; }
        public string[] PharmClassEpc { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Drugs.Entities
{

    public class RawDrug
    {
        [JsonProperty("Meta")]
        public Meta Meta { get; set; }
        [JsonProperty("Results")]
        public IEnumerable<Result> Results { get; set; }
    }

    public class Meta
    {
        [JsonProperty("Disclaimer")]
        public string Disclaimer { get; set; }
        [JsonProperty("Terms")]
        public string Terms { get; set; }
        [JsonProperty("License")]
        public string License { get; set; }
        [JsonProperty("Last_Updated")]
        public string LastUpdated { get; set; }
        [JsonProperty("Results")]
        public Results Results { get; set; }
    }

    public class Results
    {
        [JsonProperty("Skip")]
        public int Skip { get; set; }
        [JsonProperty("Limit")]
        public int Limit { get; set; }
        [JsonProperty("Total")]
        public int Total { get; set; }
    }

    public class Result
    {
        [JsonProperty("ReportType")]
        public string ReportType { get; set; }
        [JsonProperty("ReceiptDateFormat")]
        public string ReceiptDateFormat { get; set; }
        [JsonProperty("Receiver")]
        public Receiver Receiver { get; set; }
        [JsonProperty("CompanyNumb")]
        public string CompanyNumb { get; set; }
        [JsonProperty("SafetyReportVersion")]
        public string SafetyReportVersion { get; set; }
        [JsonProperty("ReceiveDateFormat")]
        public string ReceiveDateFormat { get; set; }
        [JsonProperty("PrimarySource")]
        public Primarysource PrimarySource { get; set; }
        [JsonProperty("SeriousnessOther")]
        public string SeriousnessOther { get; set; }
        [JsonProperty("Duplicate")]
        public string Duplicate { get; set; }
        [JsonProperty("TransmissionDateFormat")]
        public string TransmissionDateFormat { get; set; }
        [JsonProperty("FulfillExpediteCriteria")]
        public string FulfillExpediteCriteria { get; set; }
        [JsonProperty("SafetyReportId")]
        public string SafetyReportId { get; set; }
        [JsonProperty("Sender")]
        public Sender Sender { get; set; }
        [JsonProperty("ReceiveDate")]
        public string ReceiveDate { get; set; }
        [JsonProperty("Patient")]
        public Patient Patient { get; set; }
        [JsonProperty("TransmissionDate")]
        public string TransmissionDate { get; set; }
        [JsonProperty("Serious")]
        public string Serious { get; set; }
        [JsonProperty("ReportDuplicate")]
        public Reportduplicate ReportDuplicate { get; set; }
        [JsonProperty("ReceiptDate")]
        public string ReceiptDate { get; set; }
        [JsonProperty("PrimarySourceCountry")]
        public string PrimarySourceCountry { get; set; }
    }

    public class Receiver
    {
        [JsonProperty("ReceiverType")]
        public string ReceiverType { get; set; }
        [JsonProperty("ReceiverOrganization")]
        public string ReceiverOrganization { get; set; }
    }

    public class Primarysource
    {
        [JsonProperty("Qualification")]
        public string Qualification { get; set; }
        [JsonProperty("ReporterCountry")]
        public string ReporterCountry { get; set; }
    }

    public class Sender
    {
        [JsonProperty("SenderOrganization")]
        public string SenderOrganization { get; set; }
        [JsonProperty("SenderType")]
        public string SenderType { get; set; }
    }

    public class Patient
    {
        [JsonProperty("Reaction")]
        public IEnumerable<Reaction> Reactions { get; set; }
        [JsonProperty("PatientSex")]
        public string PatientSex { get; set; }
        [JsonProperty("Drug")]
        public IEnumerable<Drug> Drug { get; set; }
    }

    public class Reaction
    {
        [JsonProperty("ReactionMedDraVersionPt")]
        public string ReactionMedDraVersionPt { get; set; }
        [JsonProperty("ReactionMedDraPt")]
        public string ReactionMedDraPt { get; set; }
    }

    public class Drug
    {
        [JsonProperty("ActiveSubstance")]
        public ActiveSubstance ActiveSubstance { get; set; }
        [JsonProperty("DrugIndication")]
        public string DrugIndication { get; set; }
        [JsonProperty("DrugAdditional")]
        public string DrugAdditional { get; set; }
        [JsonProperty("DrugCharacterization")]
        public string DrugCharacterization { get; set; }
        [JsonProperty("DrugRecurReadministration")]
        public string DrugRecurReadministration { get; set; }
        [JsonProperty("OpenFda")]
        public OpenFda OpenFda { get; set; }
        [JsonProperty("DrugAuthorizationNumb")]
        public string DrugAuthorizationNumb { get; set; }
        [JsonProperty("MedicinalProduct")]
        public string MedicinalProduct { get; set; }
    }

    public class ActiveSubstance
    {
        [JsonProperty("ActiveSubstanceName")]
        public string ActiveSubstanceName { get; set; }
    }

    public class OpenFda
    {
        [JsonProperty("Product_Ndc")]
        public string[] ProductNdc { get; set; }
        [JsonProperty("Nui")]
        public string[] Nui { get; set; }
        [JsonProperty("Package_Ndc")]
        public string[] PackageNdc { get; set; }
        [JsonProperty("Generic_Name")]
        public string[] GenericName { get; set; }
        [JsonProperty("Spl_Set_Id")]
        public string[] SplSetId { get; set; }
        [JsonProperty("Pharm_Class_Cs")]
        public string[] PharmClassCs { get; set; }
        [JsonProperty("Brand_Name")]
        public string[] BrandName { get; set; }
        [JsonProperty("Manufacturer_Name")]
        public string[] ManufacturerName { get; set; }
        [JsonProperty("Unii")]
        public string[] Unii { get; set; }
        [JsonProperty("RxCui")]
        public string[] RxCui { get; set; }
        [JsonProperty("Spl_Id")]
        public string[] SplId { get; set; }
        [JsonProperty("Substance_Name")]
        public string[] SubstanceName { get; set; }
        [JsonProperty("Product_Type")]
        public string[] ProductType { get; set; }
        [JsonProperty("Route")]
        public string[] Route { get; set; }
        [JsonProperty("Pharm_Class_Moa")]
        public string[] PharmClassMoa { get; set; }
        [JsonProperty("Application_Number")]
        public string[] ApplicationNumber { get; set; }
        [JsonProperty("Pharm_Class_Epc")]
        public string[] PharmClassEpc { get; set; }
    }

    public class Reportduplicate
    {
        [JsonProperty("DuplicateSource")]
        public string DuplicateSource { get; set; }
        [JsonProperty("DuplicateNumb")]
        public string DuplicateNumb { get; set; }
    }
}